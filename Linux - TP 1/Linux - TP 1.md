# TP1 : On se met dans le bain

On commence avec un TP tranquille pour manipuler la ligne de commandes et l'environnement GNU/Linux.
Au menu :
- préparation de la machine en vue d'une utilisation en tant que serveur
- gestion de services (SSH, serveur Web, création de service)
On en profitera pour voir plusieurs commandes élémentaires tout du long, ainsi que des éléments de configuration récurrents sur les systèmes GNU/Linux. **On va voir, notamment** :
- revue des commandes élémentaires
- gestion d'utilisateurs et de permissions
- gestion de services
- gestion de la stack réseau
- serveur SSH

**Divisé en 3 parties : **
 - [Partie 1 : Mise en place de la solution](https://gitlab.com/Elodega/linuxrendutp/-/blob/main/Linux%20-%20TP%202/Partie%201%20-%20Mise%20en%20place%20de%20la%20solution.md)
 - [Partie 2 : Sécurisation](https://gitlab.com/Elodega/linuxrendutp/-/blob/main/Linux%20-%20TP%202/Partie%202%20-%20S%C3%A9curisation.md)
 - [Partie 3 : Maintien en condition opérationnelle](https://gitlab.com/Elodega/linuxrendutp/-/blob/main/Linux%20-%20TP%202/Partie%203%20-%20Maintien%20en%20condition%20op%C3%A9rationnelle.md)
